from django.conf import settings
from django_mako_plus import view_function
from django.http import HttpResponse, HttpResponseRedirect
from datetime import datetime
from catalog import models as cmod
from .. import dmp_render, dmp_render_to_string

@view_function
def process_request(request):
    # pid = request.urlparams[0]
    try:
        product = cmod.Product.objects.get(id=request.urlparams[0])
    except cmod.Product.DoesNotExist:
        return HttpResponseRedirect('/catalog/index/')

    # Check to see if it can remove the id, if it can, then it will remove.
    try:
        request.last5.remove(product.id)
    except ValueError:
        pass

    # Now this inserts the new id into the list. If the product was removed before, then
    # it will just insert it again.
    request.last5.insert(0, product.id)


    return dmp_render(request, 'detail.html', { 'product': product,})
