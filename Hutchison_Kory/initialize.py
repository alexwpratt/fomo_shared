import os
from datetime import datetime
# initialize the django environment
os.environ['DJANGO_SETTINGS_MODULE'] = 'fomo.settings'
import django
django.setup()
from account import models as amod #account models

# User1 = amod.FomoUser()
#
# User1.username = 'kory.hutchison'
# User1.first_name = 'Kory'
# User1.last_name = 'Hutchison'
# User1.password = 'Password1'
# User1.email = 'kory.hutchison@icloud.com'
# User1.birth_date = datetime(1994,5,19)
# User1.gender = 'Male'
# User1.save()
#
# User2 = amod.FomoUser()
#
# User2.username = 'nmarrs'
# User2.first_name = 'Nathanial'
# User2.last_name = 'Marrs'
# User2.password = 'Password1'
# User2.email = 'nathanialmarrs@gmail.com'
# User2.birth_date = datetime(1996,3,28)
# User2.gender = 'Male'
# User2.save()
#
# User3 = amod.FomoUser()
#
# User3.username = 'apratt'
# User3.first_name = 'Alex'
# User3.last_name = 'Pratt'
# User3.password = 'Password1'
# User3.email = 'alex.w.pratt@gmail.com'
# User3.birth_date = datetime(1993,5,28)
# User3.gender = 'Male'
# User3.save()
#
# User4 = amod.FomoUser()
#
# User4.username = 'brett'
# User4.first_name = 'Brett'
# User4.last_name = 'Adamson'
# User4.password = 'Password1'
# User4.email = 'adamsonb12@gmail.com'
# User4.birth_date = datetime(1993,5,11)
# User4.gender = 'Male'
# User4.save()

# Picks a specific user with the id as 2
user = amod.FomoUser.objects.get(id=2)
print(user.id, user.first_name)
print()

# Loops through all users in the database and prints out their names
for c in amod.FomoUser.objects.all():
    print(c.first_name + ' ' + c.last_name)
print()

# Filter all males and then print them out with their first name
users = amod.FomoUser.objects.filter(gender='Male')
for u in users:
    print(u.gender + ' ' + u.first_name)
print()

# Print out all the names in the database except for Nathanial
first = amod.FomoUser.objects.exclude(first_name='Nathanial')
for f in first:
    print(f.first_name)
print()
