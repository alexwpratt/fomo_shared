from django.db import models
from django.contrib.auth.models import AbstractUser

GENDER_CHOICES = [
    ['male', 'Male'],
    ['female', 'Female'],
    ['other', 'Other'],
]

class FomoUser(AbstractUser):
    # username
    # first_name
    # last_name
    # password
    # email
    # last_login
        ###The above and more are fields that are already generated and inherited
        ###from the classes
    address = models.TextField(null=True, blank=True)
    city = models.TextField(null=True, blank=True)
    state = models.TextField(null=True, blank=True)
    zipcode = models.TextField(null=True, blank=True)
    birth_date = models.DateTimeField(null=True, blank=True)
    gender = models.TextField(null=True, blank=True, choices=GENDER_CHOICES)
