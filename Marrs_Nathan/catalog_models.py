from django.db import models
from polymorphic.models import PolymorphicModel

from account import models as amod

# Create your models here. CATALOG models

class Category(models.Model):
    # id
    codename = models.TextField(blank=True, null=True, unique=True)
    name = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.name

class Product(PolymorphicModel):
    # id
    name = models.TextField(blank=True, null=True)
    category = models.ForeignKey('Category')
    price = models.DecimalField(max_digits=8, decimal_places=2) # 999,999.99
    first_image = models.TextField(blank=True, null=True)
    first_image_alttext = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    create_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)

class ProductPicture(models.Model):
    product = models.ForeignKey('Product', on_delete=models.CASCADE, related_name='images')
    path = models.TextField(blank=True, null=True)
    alttext = models.TextField(blank=True, null=True)
    mimetype = models.TextField(blank=True, null=True)
        # image/jpg, image/png, image/gif
        # Modify these in your initialize file

class BulkProduct(Product):
    # id
    quantity = models.IntegerField()
    reorder_trigger = models.IntegerField()
    reorder_quantity = models.IntegerField()

class UniqueProduct(Product):
    # id
    serial_number = models.TextField()
    available = models.BooleanField(default=True)

class RentalProduct(Product):
    # id
    serial_number = models.TextField()
    available = models.BooleanField(default=True)

############### Sprint 4 models ###############
class UserHistory(models.Model):
    # id
    product = models.ForeignKey('Product', on_delete=models.CASCADE)
    user = models.ForeignKey('account.FomoUser', on_delete=models.CASCADE, related_name='history')
    create_date = models.DateTimeField(auto_now_add=True)
    # Contains every product that the user visited, including duplicates

    # Convienence Methods
    # def getLastFive(user):
		# Logic here to get and return the last 5 viewed products

class ShoppingCart(models.Model):
    # id
    product = models.ForeignKey('Product', on_delete=models.CASCADE, related_name="cart")
    user = models.ForeignKey('account.FomoUser', on_delete=models.CASCADE, related_name='cart')
    create_date = models.DateTimeField(auto_now_add=True)
    quantity = models.IntegerField(default=1)
    # Contains only the products that the user currently has in their shopping cart

    # Convienence Methods

class Sale(models.Model):
    # id
    user = models.ForeignKey('account.FomoUser', related_name='sales')
    create_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)
    shipping = models.DecimalField(max_digits=10, decimal_places=2, default=10)
    subtotal = models.DecimalField(max_digits=10, decimal_places=2)
    total = models.DecimalField(max_digits=10, decimal_places=2)

    # Convienence Methods

    # It should create the Sale object, create SaleItem objects, update inventory and product availability, create Payment objects
    @staticmethod
    # no class object needed in the parameter
    def record_sale(user, cart_items_list, address, city, state, zipcode, stripe_charge_token):
        #  stripe api charge response (still working on STRIPE functionality)
        stripe_return = True # Currently hardcoded in
        # if valid stripe response then record a sale
        if stripe_return:
            try:
                # create the Sale object
                sale = Sale()
                sale.user = user
                sale.shipping = 10.00
                sale.subtotal = 0.0

                for item in cart_items:
                    # create SaleItem objects
                    saleitem = SaleItem()
                    saleitem.sale = sale
                    saleitem.product = item.product
                    saleitem.quantity = item.quantity
                    saleitem.taxrate = 0.0775
                    saleitem.taxamount = saleitem.taxrate * item.product.price
                    saleitem.save()

                    # update inventory and product availability
                    if hasattr(item.product, 'available'):
                        item.product.available = False
                        item.product.save()
                    if hasattr(item.product, 'quantity'):
                        item.product.quantity -= item.quantity
                        item.product.save()

                    sale.subtotal += item.product.price
                    sale.total += item.product.price + saleitem.taxamount
                sale.save()

                # create Payment object
                payment = Payment()
                payment.sale = sale
                payment.amount = sale.total
                payment.save()

            # The base class for the exceptions that are raised when a key or index used on a mapping or sequence is invalid
            except BaseException:
                return False

            # Creating shipping object to be related to the sale
            # Code only ran if there is no BaseException (improves efficiency)
            shipping = amod.ShippingAddress()
            shipping.user = user
            shipping.address = address
            shipping.city = city
            shipping.state = state
            shipping.zipcode = zipcode
            shipping.save()

            return True
        # will return true unless stripe is not valid or if there is a BaseException
        return False

class SaleItem(models.Model):
    # id
    sale = models.ForeignKey('Sale', on_delete=models.CASCADE, related_name='saleitems')
    product = models.ForeignKey('Product', related_name='saleitems')
    quantity = models.IntegerField()
    tax_rate = models.DecimalField(max_digits=10, decimal_places=2, default=.0775)
    tax_amount = models.DecimalField(max_digits=10, decimal_places=2)
    create_date = models.DateTimeField(auto_now_add=True)


class Payment(models.Model):
    # id
    sale = models.ForeignKey('Sale', related_name='payment')
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    create_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)
