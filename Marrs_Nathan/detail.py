# Django imports
from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect
from datetime import datetime

# DMP imports
from catalog import models as cmod
from django_mako_plus import view_function
from .. import dmp_render, dmp_render_to_string

@view_function
def process_request(request):
    # Get the product and the catalog related to the product
    try:
        product = cmod.Product.objects.get(id=request.urlparams[0])
        category = cmod.Category.objects.get(id=product.category.id)
    except cmod.Product.DoesNotExist:
        return HttpResponseRedirect('/catalog/index/')

    # Check for duplicate in last5products
    try:
        request.last5products.remove(product.id)
    except ValueError:
        pass

    # Add product into last5products at the beginning of the list
    request.last5products.insert(0, product.id)

    # Creating context variable to send information to the html
    context = {
        'product': product,
        'category': category,
    }

    return dmp_render(request, 'detail.html', context)
