import os
from datetime import datetime

# initialize the django environment
os.environ['DJANGO_SETTINGS_MODULE'] = 'fomo.settings'
import django
django.setup()

from account import models as amod #account models

# User1 = amod.FomoUser()
#
# User1.username = 'nmarrs'
# User1.first_name = 'Nathan'
# User1.last_name = 'Marrs'
# User1.password = 'Password 1'
# User1.email = 'nathanielmarrs@gmail.com'
# User1.birth_date = datetime(1996, 3, 28)
# User1.gender = 'Male'
# User1.save()
#
# User2 = amod.FomoUser()
#
# User2.username = 'apratt'
# User2.first_name = 'Alex'
# User2.last_name = 'Pratt'
# User2.password = 'Password 1'
# User2.email = 'apratt@gmail.com'
# User2.birth_date = datetime(1993, 4, 18)
# User2.gender = 'Male'
# User2.save()
#
# User3 = amod.FomoUser()
#
# User3.username = 'badamson'
# User3.first_name = 'Brett'
# User3.last_name = 'Adamson'
# User3.password = 'Password 1'
# User3.email = 'bAdamson@gmail.com'
# User3.birth_date = datetime(1993, 5, 11)
# User3.gender = 'Male'
# User3.save()
#
# User4 = amod.FomoUser()
#
# User4.username = 'khutch'
# User4.first_name = 'Kory'
# User4.last_name = 'Hutchison'
# User4.password = 'Password 1'
# User4.email = 'kory.hutchison@icloud.com'
# User4.birth_date = datetime(1994, 5, 19)
# User4.gender = 'Male'
# User4.save()
print()

User1 = amod.FomoUser.objects.get(id=2)
print(User1.id, User1.first_name)
print()
#Getting user info for single user

for c in amod.FomoUser.objects.all():
    print(c.first_name + ' ' + c.last_name)
print()
#Loops through all users in database and prints out their first and last name

users = amod.FomoUser.objects.filter(gender='Male')
for u in users:
    print(u.gender)
print()
#Loop through all users and display gender only if they are male (filtered)

first = amod.FomoUser.objects.filter(first_name='Nathan')
for f in first:
    print(f.first_name)
print()
#Loop through all users and displays first name if their first name is Nathan

excludefirst = amod.FomoUser.objects.exclude(first_name='Nathan')
for n in excludefirst:
    print(n.first_name)
print()
#Loops through all users and displays first name if their name is NOT Nathan

# ctrl / comments out code for you

# C#
#Question q1 = new Question()

#q1 = amod.Question()
#From there you can do q1.<whatever attribute here> and assign
#When updated, it will either create new object, or update the information

#TO get some info use
#q2 = Question.objects.get(id=5)
#print(q2.id, q2.question_text)
#
# Loop through all questions
# for c in q2.choice_set.all(): .all() gives you everything
#     print(c.choice_text)
#     print(c.question.question_text)
#
# Filter query
# questions = Question.objects.filter(question_text='Is it snowing today?')
#
# for q in questions:
#         print(q.id)
#
