def Last5ProductsMiddleware(get_response):

    def middleware(request):
        # code executed for each request before
        request.last5products = request.session.get('last5products')
        if request.last5products is None:
            request.last5products = []

        # Django will call view function here
        response = get_response(request)

        # Code to be executed for each request/response after the view is called
        request.session['last5products'] = request.last5products[:5]

        return response

    return middleware
