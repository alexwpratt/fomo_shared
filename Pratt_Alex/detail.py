from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect
from django_mako_plus import view_function
from datetime import datetime
from .. import dmp_render, dmp_render_to_string

from catalog import models as cmod

@view_function
def process_request(request):
    # get products
    try:
        product = cmod.Product.objects.get(id=request.urlparams[0])
        category = cmod.Category.objects.get(id=product.category.id)
    except cmod.Product.DoesNotExist:
        return HttpResponseRedirect('/catalog/index/')

    # get last 5 products
    last5products = []
    try:
        for last5id in request.last5:
            product_last5 = cmod.Product.objects.get(id=last5id)
            last5products.append(product_last5)
    except ValueError:
        pass

    # replace last 5 duplicate
    try:
        request.last5.remove(product.id)
    except ValueError:
        pass

    request.last5.insert(0, product.id)

    request.last5 = request.last5[0:4]

    context = {
        'product': product,
        'category': category,
        'last5products': last5products,
    }
    return dmp_render(request, 'detail.html', context)
