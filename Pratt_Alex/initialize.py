import os
from datetime import datetime

# initialize the django environment
os.environ['DJANGO_SETTINGS_MODULE'] = 'fomo.settings'
import django
django.setup()
from account import models as amod #account models

# Create 4 users in the Database
# user1 = amod.FomoUser()
# user1.username = 'alexpratt'
# user1.first_name = 'alex'
# user1.last_name = 'pratt'
# user1.set_password('alexpratt')
# user1.email = 'alex.w.pratt@gmail.com'
# user1.birth_date = datetime(1993, 5, 28)
# user1.gender = 'Male'
# user1.is_staff = True
# user1.is_admin = True
# user1.save()
#
# user2 = amod.FomoUser()
# user2.username = 'nathanmarrs'
# user2.first_name = 'nathan'
# user2.last_name = 'marrs'
# user2.set_password('nathanmarrs')
# user2.email = 'nathanielmarrs@gmail.com'
# user2.birth_date = datetime(1996, 3, 28)
# user2.gender = 'Male'
# user2.is_staff = True
# user2.is_admin = True
# user2.save()
#
# user3 = amod.FomoUser()
# user3.username = 'koryhutchison'
# user3.first_name = 'kory'
# user3.last_name = 'hutchison'
# user3.set_password('koryhutchison')
# user3.email = 'koryhutchison@icloud.com'
# user3.birth_date = datetime(1994, 5, 19)
# user3.gender = 'Male'
# user3.is_staff = True
# user3.is_admin = True
# user3.save()
#
# user4 = amod.FomoUser()
# user4.username = 'brettadamson'
# user4.first_name = 'brett'
# user4.last_name = 'adamson'
# user4.set_password(brettadamson')
# user4.email = 'adamsonb12@gmail.com'
# user4.birth_date = datetime(1993, 5, 11)
# user4.gender = 'Male'
# user4.is_staff = True
# user4.is_admin = True
# user4.save()

## query DB to get information about entered users
# all_users = amod.FomoUser.objects.all()
# for cur_user in all_users:
#     print(cur_user.first_name)
#
# user1 = amod.FomoUser.objects.get(pk=1)
# print(user1.first_name)

# male_users = amod.FomoUser.objects.filter(gender="Male")
# for cur_user in male_users:
#     print(cur_user.first_name)

not_kory = amod.FomoUser.objects.exclude(first_name="kory")
for cur_user in not_kory:
    print(cur_user.first_name)
