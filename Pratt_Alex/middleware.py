def Last5ProductsMiddleware(get_response):

    def middleware(request):

        request.last5 = request.session.get('last5')
        if request.last5 is None:
            request.last5 = []

        response = get_response(request)

        request.session['last5'] = request.last5[:5]


        return response
    
    return middleware