from django.db import models
from polymorphic.models import PolymorphicModel

from account import models as amod

class Category(models.Model):
    # id
    codename = models.TextField(blank=True, null=True)
    name = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.name

class Product((PolymorphicModel)):
    # id
    name = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    category = models.ForeignKey('Category')
    price = models.DecimalField(max_digits=8, decimal_places=2)
    create_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)

class ProductPicture(models.Model):
    product = models.ForeignKey('Product', on_delete=models.CASCADE, related_name='pictures')
    path = models.TextField(blank=True, null=True)

class BulkProduct(Product):
    # id
    quantity = models.IntegerField(blank=True, null=True)
    reorder_trigger = models.IntegerField(blank=True, null=True)
    reorder_quantity = models.IntegerField(blank=True, null=True)

class UniqueProduct(Product):
    # id
    serial_number = models.TextField()
    available = models.BooleanField(default=True)

class RentalProduct(Product):
    # id
    serial_number = models.TextField()
    available = models.BooleanField(default=True)


# S4 
class Sale(models.Model):
    # id
    user = models.ForeignKey('account.FomoUser', related_name='sales')
    create_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)
    shipping = models.DecimalField(max_digits=8, decimal_places=2, default=10)
    subtotal = models.DecimalField(max_digits=10, decimal_places=2)
    total = models.DecimalField(max_digits=10, decimal_places=2)
    

    @staticmethod
    def record_sale(user, cart_items_list, address, city, state, zipcode, stripe_charge_token):
        #  stripe api charge goes here TODO

        # with stripe response, if valid, record the sale
        stripe_return = True
        if stripe_return:
            # Create a record_sale() method in your models.py file.  This method should 
            shipping = amod.ShippingAddress()
            shipping.user = user
            shipping.address = address
            shipping.city = city
            shipping.state = state
            shipping.zipcode = zipcode
            shipping.save()
            
            try:
                # 1) create a Sale object, 
                sale = Sale()
                sale.user = user
                sale.shipping = 10.00
                sale.subtotal = 0
                for item in cart_item_list:
                    # 2) create one or more SaleItem objects for the purchases, 
                    saleitem = SaleItem()
                    saleitem.sale = sale
                    saleitem.product = item.product
                    saleitem.taxrate = 0.0725
                    # 3) create SaleItem objects for tax and shipping,
                    saleitem.taxamount = saleitem.taxrate * item.product.price
                    saleitem.quantity = item.quantity
                    saleitem.save()

                    # 5) update BulkProduct quantities and IndividualProduct availability.
                    if hasattr(item.product, 'quantity'):
                        item.product.quantity -= item.quantity
                        item.product.save()
                    if hasattr(item.product, 'available'):
                        item.product.available = False
                        item.product.save()

                    sale.subtotal += item.product.price
                    sale.total += item.product.price + saleitem.taxamount
                sale.save()

                # 4) create a Payment object,
                payment = Payment()
                payment.sale = sale
                payment.amount = sale.total
                payment.save()
            except BaseException:
                return False
            return True
        
        # if it works, it should have returned true before getting here. else return false here
        return False

class SaleItem(models.Model):
    # id
    sale = models.ForeignKey('Sale', on_delete=models.CASCADE, related_name='saleitems')
    product = models.ForeignKey('Product', related_name='saleitems')
    taxrate = models.DecimalField(max_digits=8, decimal_places=2, default=.0725)
    taxamount = models.DecimalField(max_digits=8, decimal_places=2)
    create_date = models.DateTimeField(auto_now_add=True)
    quantity = models.IntegerField()

class Payment(models.Model):
    # id
    sale = models.ForeignKey('Sale', related_name='payments')
    create_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)
    amount = models.DecimalField(max_digits=10, decimal_places=2)

class History(models.Model):
    # id
    user = models.ForeignKey('account.FomoUser', on_delete=models.CASCADE, related_name='history')
    product = models.ForeignKey('Product', on_delete=models.CASCADE)
    create_date = models.DateTimeField(auto_now_add=True)

class ShoppingCart(models.Model):
    # id
    user = models.ForeignKey('account.FomoUser', on_delete=models.CASCADE, related_name='shoppingcart')
    product = models.ForeignKey('Product', on_delete=models.CASCADE, related_name="shoppingcart")
    create_date = models.DateTimeField(auto_now_add=True)
    quantity = models.IntegerField(default=1)
